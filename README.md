﻿What works so far
-----------------

* Request reception
* Request wrapping with useful stuff
* Regex based URL routing
* URL-Handler invocation
* Session framework
* Static file responses
* Template engine (Razor!)
* Standardized response class with optional header- and/or content-buffering
* Response caching (per URL, filled manually, requires buffered responses)
* Error page upon exceptions

Roadmap
-------

In order of priority/level of interest, highest first.

* Standardized (yet customizable) error pages
* Per session URL-Handlers
* Performance tweaks (mainly for the URL matching loop)
* Something to make handling forms easier
* Something to make handling file uploads easier
