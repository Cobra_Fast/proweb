﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace proweb
{
	public static class ResponseCache
	{
		#region Subclasses
		private class CachedResponse
		{
			public string Response;
			public DateTime CachedTime;
			public TimeSpan Lifetime;
			public KeyValuePair<string, string>[] Headers;
			public ulong ETag;
		}
		#endregion

		#region Fields
		private static Dictionary<string, CachedResponse> cache = new Dictionary<string, CachedResponse>();
		private static string[] blacklistedHeaders = new string[] { "Status", "Last-Modified", "ETag", "Set-Cookie" };
		#endregion

		#region Methods
		public static void Clear()
		{
			cache.Clear();
		}

		public static bool Invalidate(Request rq)
		{
			return cache.Remove(rq.Headers["REQUEST_URI"]);
		}

		public static bool Invalidate(string Url)
		{
			return cache.Remove(Url);
		}

		public static bool Cache(Request rq, Response re, TimeSpan? Lifetime = null)
		{
			if (!(re.Buffering == Response.BufferModes.Body || re.Buffering == Response.BufferModes.Full))
				return false;

			if (rq.Body.Length > 0)
				return false;

			CachedResponse cr = new CachedResponse() { CachedTime = DateTime.Now, Lifetime = (Lifetime.HasValue ? Lifetime.Value : new TimeSpan(0)) };
			re.Buffer.Position = 0;
			using (StreamReader sr = new StreamReader(re.Buffer, Encoding.UTF8, false, 4096, true))
				cr.Response = sr.ReadToEnd();
			cr.ETag = Crc64.ComputeChecksum(cr.Response);

			if (re.Buffering == Response.BufferModes.Full)
			{
				List<KeyValuePair<string, string>> hc = new List<KeyValuePair<string, string>>();
				foreach (KeyValuePair<string, string> h in re.Headers)
				{
					if (!blacklistedHeaders.Contains(h.Key))
						hc.Add(h);
				}
				cr.Headers = hc.ToArray();
			}

			if (cache.ContainsKey(rq.Headers["REQUEST_URI"]))
				cache.Remove(rq.Headers["REQUEST_URI"]);
			cache.Add(rq.Headers["REQUEST_URI"], cr);
			return true;
		}

		public static bool TrySatisfy(Request rq)
		{
			if (cache.ContainsKey(rq.Headers["REQUEST_URI"]))
			{
				CachedResponse cr = cache[rq.Headers["REQUEST_URI"]];
				if (cr.Lifetime.Ticks == 0 || cr.CachedTime.Add(cr.Lifetime) > DateTime.Now)
				{
					rq.Response.Buffering = Response.BufferModes.Headers;
					rq.Response.AddHeader("Last-Modified", cr.CachedTime.ToStringHttp());
					rq.Response.AddHeader("ETag", cr.ETag.ToString(CultureInfo.InvariantCulture));

					foreach (KeyValuePair<string, string> h in cr.Headers)
						rq.Response.AddHeader(h.Key, h.Value);

					if (rq.Headers["HTTP_IF_NONE_MATCH"] != null &&
						rq.Headers["HTTP_IF_NONE_MATCH"] == cr.ETag.ToString(CultureInfo.InvariantCulture))
					{
						rq.Response.SetStatus(304);
						rq.Response.Write("");
					}
					else
					{
						rq.Response.SetStatus(200);
						rq.Response.AddHeader("Content-Length", cr.Response.Length.ToString(CultureInfo.InvariantCulture));
						rq.Response.Write(cr.Response);
					}

					rq.Response.Close();
					return true;
				}
				
				cache.Remove(rq.Headers["REQUEST_URI"]);
			}

			return false;
		}
		#endregion
	}
}
