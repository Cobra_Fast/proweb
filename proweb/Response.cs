﻿using System;
using System.Collections.Generic;
using System.IO;
using RazorEngine;
using RazorEngine.Compilation.ImpromptuInterface.Dynamic;
using Encoding = System.Text.Encoding;

namespace proweb
{
	public class Response
	{
		#region Fields
		private Dictionary<string, string> headers = new Dictionary<string, string>();
		private List<string> header_setcookie = new List<string>(); 
		private Stream responseStream;
		private MemoryStream bufferedResponseStream = new MemoryStream();
		private bool headersSent;
		#endregion

		#region Enums
		public enum BufferModes
		{
			None, Headers, Body, Full
		}

		public static readonly Dictionary<ushort, string> StatusMessages = new Dictionary<ushort, string>
		{
			{200, "OK"},
			{201, "Created"},
			{202, "Accepted"},
			{203, "Non-Authoritative Information"},
			{204, "No Content"},
			{205, "Reset Content"},
			{206, "Partial Content"},
			{207, "Multi-Status"},
			{208, "Already Reported"},
			{226, "IM Used"},
			{300, "Multiple Choices"},
			{301, "Moved Permanently"},
			{302, "Found"},
			{303, "See Other"},
			{304, "Not Modified"},
			{305, "Use Proxy"},
			{306, "Switch Proxy"},
			{307, "Temporary Redirect"},
			{308, "Permanent Redirect"},
			{400, "Bad Request"},
			{401, "Unauthorized"},
			{402, "Payment Required"},
			{403, "Forbidden"},
			{404, "Not Found"},
			{405, "Method Not Allowed"},
			{406, "Not Acceptable"},
			{407, "Proxy Authentication Required"},
			{408, "Request Timeout"},
			{409, "Conflict"},
			{410, "Gone"},
			{411, "Length Required"},
			{412, "Precondition Failed"},
			{413, "Request Entity Too Large"},
			{414, "Request-URI Too Long"},
			{415, "Unsupported Media Type"},
			{416, "Requested Range Not Satisfiable"},
			{417, "Expectation Failed"},
			{418, "I'm a teapot"},
			{419, "Authentication Timeout"},
			{420, "Method Failure"},
			{422, "Unprocessable Entity"},
			{423, "Locked"},
			{424, "Failed Dependency"},
			{426, "Upgrade Required"},
			{428, "Precondition Required"},
			{429, "Too Many Requests"},
			{431, "Request Header Fields Too Large"},
			{440, "Login Timeout"},
			{444, "No Response"},
			{449, "Retry With"},
			{450, "Blocked by Windows Parental Control"},
			{451, "Unavailable For Legal Reasons"},
			{494, "Request Header Too Large"},
			{495, "Cert Error"},
			{496, "No Cert"},
			{497, "HTTP to HTTPS"},
			{498, "Token expired/invalid"},
			{499, "Client Closed Request"},
			{500, "Internal Server Error"},
			{501, "Not Implemented"},
			{502, "Bad Gateway"},
			{503, "Service Unavailable"},
			{504, "Gateway Timeout"},
			{505, "HTTP Version Not Supported"},
			{506, "Variant Also Negotiates"},
			{507, "Insufficient Storage"},
			{508, "Loop Detected"},
			{509, "Bandwidth Limit Exceeded"},
			{510, "Not Extended"},
			{511, "Network Authentication Required"},
			{598, "Network read timeout error"},
			{599, "Network connect timeout error"}
		};
		#endregion

		#region Properties
		public BufferModes Buffering = BufferModes.Headers;

		public Stream Stream
		{
			get
			{
				if (this.Buffering == BufferModes.Body || this.Buffering == BufferModes.Full)
					return this.bufferedResponseStream;
				return this.responseStream;
			}
		}

		public Stream DirectStream
		{
			get { return this.responseStream; }
		}

		public Stream BufferedStream
		{
			get { return this.bufferedResponseStream; }
		}

		public MemoryStream Buffer
		{
			get { return this.bufferedResponseStream; }
		}

		public Dictionary<string, string> Headers
		{
			get { return this.headers; }
		}

		public List<string> Cookies
		{
			get { return this.header_setcookie; }
		}
		#endregion

		#region Constructors
		public Response(Request rq)
		{
			this.responseStream = rq.ScgiChildResponseStream;
		}
		#endregion

		#region Methods
		public void AddHeader(string Header, string Value)
		{
			if (this.headersSent)
				throw new InvalidOperationException("Headers were already sent.");

			if (this.Buffering == BufferModes.Headers || this.Buffering == BufferModes.Full)
			{
				if (this.headers.ContainsKey(Header))
					this.headers.Remove(Header);
			}
			else
			{
				if (this.headers.ContainsKey(Header))
					throw new InvalidOperationException("This header has already been sent.");
				this.Stream.WriteLine(Header + ": " + Value);
			}

			this.headers.Add(Header, Value);
		}

		public void AddHeader(string HeaderAndValue)
		{
			string[] d = HeaderAndValue.Split(new char[] {':'}, 2);
			this.AddHeader(d[0], d[1]);
		}

		public void SetCookie(string Name, string Value, DateTime? ExpiryDate = null, string Path = "", string Domain = "", bool RequireSecure = false, bool HttpOnly = false)
		{
			if (this.headersSent)
				throw new InvalidOperationException("Headers were already sent.");

			string cookie = Name + "=" + Value + ";";
			if (ExpiryDate.HasValue)
				cookie += "Expires=" + ExpiryDate.Value.ToStringHttp() + ";";
			if (Path != "")
				cookie += "Path=" + Path + ";";
			if (Domain != "")
				cookie += "Domain=" + Domain + ";";
			if (RequireSecure)
				cookie += "Secure;";
			if (HttpOnly)
				cookie += "HttpOnly;";

			if (this.Buffering == BufferModes.Headers || this.Buffering == BufferModes.Full)
				this.header_setcookie.Add(cookie);
			else
				this.Stream.WriteLine("Set-Cookie: " + cookie);
		}

		public void SetStatus(ushort Code)
		{
			this.AddHeader("Status", Code + Response.StatusMessages[Code]);
		}

		public void SetStatus(string Code)
		{
			this.AddHeader("Status", Code);
		}

		public void Write(byte[] Data)
		{
			if (!this.headersSent)
				this.sendHeaders();
			this.Stream.Write(Data);
		}

		public void Write(string Data)
		{
			if (!this.headersSent)
				this.sendHeaders();
			this.Stream.Write(Data);
		}

		public void WriteLine(string Data)
		{
			if (!this.headersSent)
				this.sendHeaders();
			this.Stream.WriteLine(Data);
		}

		public void Close()
		{
			if (this.Buffering == BufferModes.Body || this.Buffering == BufferModes.Full)
			{
				this.bufferedResponseStream.Position = 0;
				using (StreamReader r = new StreamReader(this.bufferedResponseStream, Encoding.UTF8, false, 4096, true))
				{
					this.DirectStream.Write(r.ReadToEnd());
				}
			}

			this.DirectStream.Close();
		}

		public void SendHeaders()
		{
			if (!this.headersSent)
				this.sendHeaders();
		}

		protected void sendHeaders()
		{
			if (this.headersSent)
				throw new InvalidOperationException("Headers were already sent.");

			if (this.Buffering == BufferModes.Headers || this.Buffering == BufferModes.Full)
			{
				// status header must be the first according to SCGI
				if (!this.headers.ContainsKey("Status"))
					this.SetStatus("200 OK (assumed)");
				this.DirectStream.WriteLine("Status: " + this.headers["Status"]);
				this.headers.Remove("Status");

				foreach (KeyValuePair<string, string> header in this.headers)
					this.DirectStream.WriteLine(header.Key + ": " + header.Value);
				foreach (string cookie in this.header_setcookie)
					this.DirectStream.WriteLine("Set-Cookie: " + cookie);
			}

			this.DirectStream.WriteLine("");
			this.headersSent = true;
		}
		#endregion
	}
}
