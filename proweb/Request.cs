﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using scginet;

namespace proweb
{
	public class Request
	{
		#region Fields
		private ScgiChild sc;
		private NameValueCollection __postbody_cache;
		private NameValueCollection __querystring_cache;
		private NameValueCollection __cookies_cache;
		private Uri __url_cache;
		private Response re;
		#endregion

		#region Properties
		public Response Response
		{
			get { return this.re; }
		}

		public NameValueCollection Headers
		{
			get { return this.sc.Headers; }
		}

		public byte[] Body
		{
			get { return this.sc.Body; }
		}

		public Stream ScgiChildResponseStream
		{
			get { return this.sc.Response; }
		}

		public Uri Url
		{
			get
			{
				if (this.__url_cache == null)
					return (this.__url_cache = new Uri("http://" + this.sc.Headers["HTTP_HOST"] + ":" + this.sc.Headers["SERVER_PORT"] + this.sc.Headers["REQUEST_URI"]));
				return this.__url_cache;
			}
		}
		#endregion

		#region Constructors
		public Request(ScgiChild sc)
		{
			this.sc = sc;
			if (!this.sc.UriIsWellFormatted())
				throw new FormatException("Request URL not legitimate");

			this.re = new Response(this);
		}
		#endregion

		#region Methods
		public NameValueCollection GetCookies()
		{
			if (this.Headers["HTTP_COOKIE"] != null)
			{
				if (this.__cookies_cache != null)
					return this.__cookies_cache;

				NameValueCollection nvc = new NameValueCollection();
				foreach (string c in this.Headers["HTTP_COOKIE"].Split(';'))
				{
					string[] d = c.Split(new char[] {'='}, 2);
					nvc.Add(d[0], d[1]);
				}
				return (this.__cookies_cache = nvc);
			}

			return new NameValueCollection();
		}

		public NameValueCollection GetPostBody()
		{
			if (this.__postbody_cache == null)
				return (this.__postbody_cache = this.sc.ParsePostBody());
			return this.__postbody_cache;
		}

		public NameValueCollection GetQueryString()
		{
			if (this.__querystring_cache == null)
				return (this.__querystring_cache = this.sc.ParseQueryString());
			return this.__querystring_cache;
		}

		public void Close()
		{
			this.re.Close();
			this.sc.Close();
		}
		#endregion
	}
}
