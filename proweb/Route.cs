﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace proweb
{
	public class Route
	{
		#region Fields
		public Regex UrlRegex;
		public IRouteHandler Handler;
		#endregion

		#region Constructors
		public Route(string UrlRegex, IRouteHandler Handler)
		{
			this.UrlRegex = new Regex(UrlRegex, RegexOptions.Compiled);
			this.Handler = Handler;
		}

		public Route(Regex UrlRegex, IRouteHandler Handler)
		{
			this.UrlRegex = UrlRegex;
			this.Handler = Handler;
		}
		#endregion
	}
}
