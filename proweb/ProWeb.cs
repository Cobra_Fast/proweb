﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RazorEngine;
using RazorEngine.Templating;
using scginet;

namespace proweb
{
	public class ProWeb
	{
		#region Fields
		private RouteTable routetable;
		private Scgi scgis;
		#endregion

		#region Properties
		public RouteTable Routes
		{
			get { return this.routetable; }
			set { this.routetable = value; }
		}
		#endregion

		#region Constructors
		public ProWeb(Scgi SCGIServer)
		{
			this.scgis = SCGIServer;

			this.routetable = new RouteTable();

			this.scgis.Request += scgis_Request;
		}
		#endregion

		#region Methods
		void scgis_Request(object sender, EventArgs e)
		{
			ScgiChild sc = sender as ScgiChild;

			if (sc == null)
				throw new InvalidDataException("Request sender object is not an SCGI request.");

			try
			{
				Request rq = new Request(sc);

				this.Routes.HandleUrl(rq);
			}
			catch (Exception x)
			{
				const string xp = @"<!DOCTYPE html>
<html>
	<head>
		<title>500 Internal Server Error</title>
		<style type='text/css'>
			HTML { margin: 0; padding: 0.67em; }
			BODY { margin: 0; padding: 1em; font-family: sans-serif; font-size: 9pt; border-left: 1px solid #f00; }
			PRE { background: #efefef; padding: 8px; }
			H1 { margin-top: 0; }
		</style>
	</head>
	<body>
		<h1>500 Internal Server Error</h1>
		<h3>@(Model.GetType().ToString())<text> occurred</text></h3>
		<p>@Model.Message</p>
		<pre><text>in </text>@Model.Source
@Model.StackTrace</pre>
		@if (Model.Data != null && Model.Data.Count > 0)
		{
			<fieldset>
				<table>
				@foreach (KeyValuePair<object, object> kvp in Model.Data)
				{
					<tr>
						<td>@(kvp.Key.ToString())</td>
						<td>@(kvp.Value.ToString())</td>
					</tr>
				}
				</table>
			</fieldset>
		}
	</body>
</html>";
				string o = Engine.Razor.RunCompile(xp, @"_dflt_error", typeof(Exception), x);
				sc.Response.WriteLine("Status: 500 Internal Server Error");
				sc.Response.WriteLine("Content-Type: text/html");
				sc.Response.WriteLine("Content-Length: " + o.Length);
				sc.Response.WriteLine("Pragma: no-cache");
				sc.Response.WriteLine("");
				sc.Response.Write(o);
				sc.Close();
			}
		}
		#endregion
	}
}
