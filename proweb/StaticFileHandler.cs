﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace proweb
{
	public class StaticFileHandler : IRouteHandler
	{
		#region Fields
		private Dictionary<string, ETag> etagcache = new Dictionary<string, ETag>(); 
		#endregion

		#region Structs
		protected struct ETag
		{
			public ulong Hash;
			public DateTime DateHashed;
		}
		#endregion

		#region Tables
		private static Dictionary<string, string> ext2mime = new Dictionary<string, string>()
		{
			{"jpg", "image/jpeg"},
			{"png", "image/png"},
			{"gif", "image/gif"},
			{"js", "text/javascript"},
			{"css", "text/css"},
			{"html", "text/html"},
			{"txt", "text/plain"},
			{"json", "application/json"}
		};

		private static string defaultmime = "text/plain";
		#endregion

		#region Properties
		public string FileSeekPathPrefix = @".";
		#endregion

		#region Constructors
		public StaticFileHandler()
		{ }

		public StaticFileHandler(string FileSeekPathPrefix)
		{
			this.FileSeekPathPrefix = FileSeekPathPrefix;
		}
		#endregion

		#region Methods
		public virtual void Handle(Request sc, Match url)
		{
			FileInfo f = new FileInfo(this.FileSeekPathPrefix + url.Value);
			this.HandleFile(sc, f);
			sc.Close();
		}

		protected void HandleFile(Request sc, FileInfo f)
		{
			if (f.Exists)
			{
				if (sc.Headers["HTTP_IF_NONE_MATCH"] != null && this.etagcache.ContainsKey(f.FullName))
				{
					ETag _etag = this.etagcache[f.FullName];
					if (_etag.Hash.ToString(CultureInfo.InvariantCulture) == sc.Headers["HTTP_IF_NONE_MATCH"].Trim())
					{
						sc.Response.Buffering = Response.BufferModes.None;
						sc.Response.SetStatus(304);
						sc.Response.AddHeader("Last-Modified", f.LastWriteTime.ToStringHttp());
						sc.Response.AddHeader("ETag", _etag.Hash.ToString(CultureInfo.InvariantCulture));
						sc.Response.Write("");
						return;
					}
				}

				ETag etag = this.GetFileETag(f);
				if (!this.etagcache.ContainsKey(f.FullName))
					this.etagcache.Add(f.FullName, etag);

				sc.Response.Buffering = Response.BufferModes.Headers;
				sc.Response.SetStatus(200);
				sc.Response.AddHeader("Content-Type", this.GetMimeType(f));
				sc.Response.AddHeader("Content-Length", f.Length.ToString(CultureInfo.InvariantCulture));
				sc.Response.AddHeader("Last-Modified", f.LastWriteTime.ToStringHttp());
				sc.Response.AddHeader("ETag", etag.Hash.ToString(CultureInfo.InvariantCulture));
				sc.Response.SendHeaders();

				using (FileStream fs = f.OpenRead())
				{
					fs.CopyTo(sc.Response.Stream);
				}
			}
			else
			{
				sc.Response.SetStatus(404);
				sc.Response.WriteLine(String.Format("The requested file {0} could not be found.", f.FullName));
			}
		}

		protected ETag GetFileETag(FileInfo file)
		{
			using (FileStream fs = file.OpenRead())
			using (Crc64 c64 = new Crc64())
			{
				ETag etag = new ETag();
				byte[] hash = c64.ComputeHash(fs);
				etag.Hash = BitConverter.ToUInt64(hash, 0);
				etag.DateHashed = DateTime.Now;
				return etag;
			}
		}

		protected string GetMimeType(FileInfo file)
		{
			if (file.Extension == "")
				return StaticFileHandler.defaultmime;

			string ext = file.Extension.Substring(1);
			if (StaticFileHandler.ext2mime.ContainsKey(ext))
				return StaticFileHandler.ext2mime[ext];
			return StaticFileHandler.defaultmime;
		}

		/// <summary>
		/// Loads a file that contains a list of file extensions mapped to MIME-types.
		/// </summary>
		/// <param name="listfile">File that contains the list.</param>
		/// <param name="extendCurrent">Set to true to extend the stock/default or previously loaded table.</param>
		/// <param name="separator">The char that separates the file extension from the mime type in each line.</param>
		protected void LoadMimeExtensionList(FileInfo listfile, bool extendCurrent = false, char separator = ' ')
		{
			if (!extendCurrent)
				StaticFileHandler.ext2mime.Clear();

			FileStream fs = listfile.OpenRead();
			StreamReader r = new StreamReader(fs);
			while (!r.EndOfStream)
			{
				string l = r.ReadLine();
				if (l == null)
					continue;

				string[] d = l.Split(new char[] { separator }, 2);

				if (extendCurrent && StaticFileHandler.ext2mime.ContainsKey(d[0]))
					continue;

				StaticFileHandler.ext2mime.Add(d[0], d[1]);
			}
		}
		#endregion
	}
}
