﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proweb
{
	public static class StreamHelper
	{
		public static void Write(this Stream s, byte[] Data)
		{
			s.Write(Data, 0, Data.Length);
		}

		public static void Write(this Stream s, string Data)
		{
			byte[] b = Encoding.UTF8.GetBytes(Data);
			s.Write(b, 0, b.Length);
		}

		public static void WriteLine(this Stream s, string Data)
		{
			s.Write(Data + "\r\n");
		}

		public static string ReadLine(this Stream s)
		{
			const int chunksize = 64;
			byte[] buffer = new byte[chunksize];
			int index = 0;

			int input;

			while ((input = s.ReadByte()) >= 0)
			{
				buffer[index++] = (byte)input;

				if (index >= 2 && buffer[index - 2] == 13 && buffer[index - 1] == 10)
					return Encoding.UTF8.GetString(buffer, 0, index - 2);

				if (index >= 1 && buffer[index - 1] == 10)
					return Encoding.UTF8.GetString(buffer, 0, index - 1);

				if (index == buffer.Length)
					Array.Resize(ref buffer, buffer.Length + chunksize);
			}

			if (index == 0)
				throw new EndOfStreamException();

			return Encoding.UTF8.GetString(buffer, 0, index);
		}
	}
}
