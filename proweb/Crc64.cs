﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

// namespace Iridescence
// thanks
namespace proweb
{
	/// <summary>
	/// Implements a HashAlgorithm that computes CRC-64 checksums.
	/// </summary>
	public sealed class Crc64 : HashAlgorithm
	{
		#region Fields

		public const ulong defaultPolynomial = 0xC96C5795D7870F42ul;

		public static readonly Crc64 Default;

		internal readonly ulong[] Table;

		private readonly ulong polynomial;
		private ulong hash;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the polynomial of this CRC algorithm.
		/// </summary>
		public ulong Polynomial
		{
			get { return this.polynomial; }
		}

		/// <summary>
		/// Gets the size of the hash in bytes.
		/// </summary>
		public override int HashSize
		{
			get { return 8; }
		}

		#endregion

		#region Constructors

		static Crc64()
		{
			Default = new Crc64();
		}

		/// <summary>
		/// Creates a new CRC-64 hash algorithm using the default polynomial.
		/// </summary>
		public Crc64()
			: this(defaultPolynomial)
		{

		}

		/// <summary>
		/// Creates a new CRC-64 hash algorithm using the specified polynomial.
		/// </summary>
		/// <param name="polynomial"></param>
		public Crc64(ulong polynomial)
		{
			this.polynomial = polynomial;
			this.Table = new ulong[256];

			for (ulong i = 0; i < 256; i++)
			{
				ulong entry = i;

				for (var j = 0; j < 8; j++)
					if ((entry & 1) == 1)
						entry = (entry >> 1) ^ polynomial;
					else
						entry = entry >> 1;

				this.Table[i] = entry;
			}

			this.HashSizeValue = 8;

			this.Initialize();
		}

		#endregion

		#region Methods

		#region Instance

		/// <summary>
		/// Initializes the hash algorithm.
		/// </summary>
		public override void Initialize()
		{
			this.hash = ~0ul;
		}

		/// <summary>
		/// Computes the hash for the specified data.
		/// </summary>
		/// <param name="array"></param>
		/// <param name="ibStart"></param>
		/// <param name="cbSize"></param>
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			for (int i = 0; i < cbSize; i++)
				this.hash = (this.hash >> 8) ^ Table[array[ibStart + i] ^ this.hash & 0xFF];
		}

		/// <summary>
		/// Finalizes and returns the hash.
		/// </summary>
		/// <returns></returns>
		protected override byte[] HashFinal()
		{
			return BitConverter.GetBytes(~this.hash);
		}

		#endregion

		#region Static

		/// <summary>
		/// Computes the CRC-64 checksum for the specified byte array.
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public static ulong ComputeChecksum(byte[] bytes)
		{
			ulong crc = ~0u;
			for (int i = 0; i < bytes.Length; ++i)
				crc = (crc >> 8) ^ Default.Table[bytes[i] ^ crc & 0xFF];
			return ~crc;
		}

		/// <summary>
		/// Computes the CRC-64 checksum for the specified string using UTF-8 encoding.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static ulong ComputeChecksum(string str)
		{
			return ComputeChecksum(Encoding.UTF8.GetBytes(str));
		}

		/// <summary>
		/// Reads all bytes from the stream and returns the CRC-64 checksum.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static ulong ComputeChecksum(Stream stream)
		{
			ulong crc = ~0ul;
			byte[] buffer = new byte[65536];
			int numBytes;

			while ((numBytes = stream.Read(buffer, 0, buffer.Length)) > 0)
			{
				for (int i = 0; i < numBytes; ++i)
					crc = (crc >> 8) ^ Default.Table[buffer[i] ^ crc & 0xFF];
			}

			return ~crc;
		}

		#endregion

		#endregion
	}
}
