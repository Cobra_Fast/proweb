﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Threading;

namespace proweb
{
	public class SessionManager
	{
		#region Fields
		private Dictionary<string, Session> sessions = new Dictionary<string, Session>();
		private Timer gc;
		#endregion

		#region Enums
		public enum SessionCachingOpt { InMemory, InFile, Dynamic };
		#endregion

		#region Properties
		/// <summary>
		/// Which caching strategy for sessions should be used.
		/// In Memory: (Default) Session data is always kept in memory.
		/// In File: Session data is always written to file.
		/// Dynamic: Session data is kept in memory for a given time, then written to file.
		/// </summary>
		public SessionCachingOpt SessionCaching = SessionCachingOpt.InMemory;

		/// <summary>
		/// The directory in which session data should be stored.
		/// </summary>
		public DirectoryInfo SessionCachingFileDirectory;

		/// <summary>
		/// TimeSpan since last access after which a session expires. (Default: 3h)
		/// </summary>
		public TimeSpan SessionCachingMaxAge = new TimeSpan(0, 3, 0, 0);

		/// <summary>
		/// TimeSpan since last access after which a session should be moved from memory to file.
		/// </summary>
		public TimeSpan SessionCachingDynamicMaxMemoryAge;

		/// <summary>
		/// Interval in which the GC thread will check for old sessions. Run SessionManager.RunGC() to apply changes.
		/// </summary>
		public TimeSpan SessionCachingGCInterval = new TimeSpan(0, 0, 5, 0);

		/// <summary>
		/// Name of the session cookie.
		/// </summary>
		public string SessionCookieName = "PHPSESSID";
		#endregion

		#region Constructors
		public SessionManager()
		{
			this.gc = new Timer(this.gc_run, null, new TimeSpan(0), this.SessionCachingGCInterval);
		}
		#endregion

		#region Methods
		private void gc_run(object state)
		{
			foreach (Session s in this.sessions.Values.ToArray())
			{
				if (DateTime.Now.Subtract(s.AccessTime) > this.SessionCachingMaxAge)
				{
					this.sessions.Remove(s.ID);
					s.Delete();
				}

				if (this.SessionCaching == SessionCachingOpt.Dynamic)
				{
					if (!s.IsHibernated && DateTime.Now.Subtract(s.AccessTime) > this.SessionCachingDynamicMaxMemoryAge)
						s.Hibernate();
				}
				else if (this.SessionCaching == SessionCachingOpt.InFile)
				{
					if (!s.IsHibernated)
						s.Hibernate();
				}
			}
		}

		/// <summary>
		/// Starts the internal GC timer.
		/// </summary>
		/// <param name="runonce">Set to true if GC should be run only once and/or disable GC, then this method needs to be called again.</param>
		public void RunGC(bool runonce = false)
		{
			if (runonce)
				this.gc.Change(0, -1);
			else
				this.gc.Change(new TimeSpan(0), this.SessionCachingGCInterval);
		}

		/// <summary>
		/// Creates a session with session options applied.
		/// Don't forget to set the cookie.
		/// </summary>
		/// <returns>The newly created session.</returns>
		public Session CreateSession()
		{
			Session s;

			if (this.SessionCaching == SessionCachingOpt.InFile || this.SessionCaching == SessionCachingOpt.Dynamic)
			{
				string id = this.generateSessionId();
				s = new Session(id, new FileInfo(this.SessionCachingFileDirectory.FullName + @"/" + id + ".dat"));
			}
			else
				s = new Session(this.generateSessionId());

			this.sessions.Add(s.ID, s);
			return s;
		}

		/// <summary>
		/// Invokes Request.SetCookie() with the appropriate values to create the session cookie.
		/// Needs to be called while headers are still being written to the response.
		/// </summary>
		/// <param name="r">Request to whichs response the cookie will be appended.</param>
		/// <param name="s">Session whichs ID should be written to the cookie.</param>
		public void CreateSessionCookie(Response r, Session s)
		{
			r.SetCookie(this.SessionCookieName, s.ID);
		}

		/// <summary>
		/// Attempts to retrieve a session by ID from the manager's session dictionary.
		/// </summary>
		/// <param name="ID">Session ID to search for.</param>
		/// <returns>The Session object if found or null if not found.</returns>
		public Session GetSession(string ID)
		{
			Session s;
			if (this.sessions.TryGetValue(ID, out s))
			{
				s.AccessTime = DateTime.Now;
				return s;
			}
			return null;
		}

		/// <summary>
		/// Attempts to read the session cookie from a request and retrieve the according session from the manager's session dictionary.
		/// </summary>
		/// <param name="r">The request in question.</param>
		/// <returns>The session object if found or null if not found.</returns>
		public Session GetSession(Request r)
		{
			NameValueCollection cookies = r.GetCookies();
			if (cookies[this.SessionCookieName] != null)
				return this.GetSession(cookies[this.SessionCookieName]);
			return null;
		}

		private string generateSessionId(int length = 16)
		{
			string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
			Random r = new Random();
			return new string(Enumerable.Repeat(chars, length).Select(s => s[r.Next(s.Length)]).ToArray());
		}
		#endregion
	}
}
