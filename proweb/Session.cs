﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace proweb
{
	public class Session
	{
		#region Fields
		private string id;
		private object data;
		private FileInfo cache;
		private DateTime accessTime;
		private bool ishibernated;
		#endregion

		#region Properties
		public string ID
		{
			get { return this.id; }
			set { this.id = value; }
		}

		public object Data
		{
			get
			{
				if (this.ishibernated && this.data == null)
					this.Wakeup();
				return this.data;
			}
			set { this.data = value; }
		}

		public DateTime AccessTime
		{
			get { return this.accessTime; }
			set { this.accessTime = value; }
		}

		public bool IsHibernated
		{
			get { return this.ishibernated; }
		}
		#endregion

		#region Constructors
		public Session(string id) : this(id, null, null)
		{ }

		public Session(string id, object data) : this(id, data, null)
		{ }

		public Session(string id, FileInfo cache) : this(id, null, cache)
		{ }

		public Session(string id, object data, FileInfo cache)
		{
			this.id = id;
			this.data = data;
			this.cache = cache;
		}
		#endregion

		#region Methods
		/// <summary>
		/// Loads session data from hibernation file.
		/// </summary>
		public void Wakeup()
		{
			if (this.cache == null)
				throw new NotSupportedException("No session cache file was set up, hence hibernation is not supported.");
			if (!this.cache.Exists)
				return;

			IFormatter f = new BinaryFormatter();
			Stream s = new FileStream(this.cache.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
			this.data = f.Deserialize(s);
			this.accessTime = DateTime.Now;
			this.ishibernated = false;
		}

		/// <summary>
		/// Saves session data to hibernation file and nulls data field.
		/// </summary>
		public void Hibernate()
		{
			if (this.cache == null)
				throw new NotSupportedException("No session cache file was set up, hence hibernation is not supported.");
			if (this.ishibernated)
				return;
			if (this.data == null)
				return;

			IFormatter f = new BinaryFormatter();
			Stream s = new FileStream(this.cache.FullName, FileMode.Create, FileAccess.Write, FileShare.None);
			f.Serialize(s, this.data);
			s.Close();

			this.data = null;
			this.ishibernated = true;
		}

		public void Delete()
		{
			if (this.cache != null)
				this.cache.Delete();
			this.data = null;
		}
		#endregion
	}
}
