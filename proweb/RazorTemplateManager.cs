﻿using System;
using System.IO;
using RazorEngine;
using RazorEngine.Templating;

namespace proweb
{
	public class RazorTemplateManager : ITemplateManager
	{
		#region Fields
		private string tplpathprefix;
		private readonly System.Collections.Concurrent.ConcurrentDictionary<ITemplateKey, ITemplateSource> _dynamicTemplates =
			new System.Collections.Concurrent.ConcurrentDictionary<ITemplateKey, ITemplateSource>();
		#endregion

		#region Properties
		#endregion

		#region Constructors
		public RazorTemplateManager(string TplPathPrefix = "./")
		{
			this.tplpathprefix = TplPathPrefix;
		}
		#endregion

		#region Methods
		public ITemplateSource Resolve(ITemplateKey key)
		{
			if (_dynamicTemplates.ContainsKey(key))
				return _dynamicTemplates[key];

			FileInfo fi = new FileInfo(this.tplpathprefix + key.Name);
			using (FileStream fs = fi.OpenRead())
			using (StreamReader r = new StreamReader(fs))
			{
				return new LoadedTemplateSource(r.ReadToEnd(), fi.FullName);
			}
		}

		public ITemplateKey GetKey(string name, ResolveType resolveType, ITemplateKey context)
		{
			return new NameOnlyTemplateKey(name, resolveType, context);
		}

		public void AddDynamic(ITemplateKey key, ITemplateSource source)
		{
			_dynamicTemplates.AddOrUpdate(key, source, (k, oldSource) =>
			{
				if (oldSource.Template != source.Template)
				{
					throw new InvalidOperationException("The same key was already used for another template!");
				}
				return source;
			});
		}
		#endregion
	}
}
