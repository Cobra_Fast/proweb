﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using scginet;

namespace proweb
{
	public class RouteTable : List<Route>
	{
		public RouteTable() : base()
		{ }

		#region Methods
		// TODO lots of headroom for optimization here
		public bool HandleUrl(Request rq)
		{
			if (ResponseCache.TrySatisfy(rq))
				return true;

			foreach (Route r in this)
			{
				Match m = r.UrlRegex.Match(rq.Url.AbsolutePath);
				if (m.Success)
				{
					r.Handler.Handle(rq, m);
					return true;
				}
			}

			return false;
		}
		#endregion
	}
}
