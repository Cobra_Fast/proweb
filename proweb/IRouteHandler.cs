﻿using System;
using System.Text.RegularExpressions;

namespace proweb
{
	public interface IRouteHandler
	{
		void Handle(Request sc, Match url);
	}
}
