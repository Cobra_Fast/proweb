﻿using System;
using System.Globalization;
using System.IO;
using RazorEngine;
using RazorEngine.Compilation;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using RazorEngine.Text;

namespace proweb
{
	public static class RazorHelper
	{
		#region Methods
		public static void ApplyProWebConfiguration(bool Debug = false, string TemplatePathPrefix = "./")
		{
			var cfg = new TemplateServiceConfiguration();
			cfg.Debug = Debug;
			cfg.Language = Language.CSharp;
			cfg.EncodedStringFactory = new RawStringFactory();
			cfg.TemplateManager = new RazorTemplateManager(TemplatePathPrefix);
			cfg.CachingProvider = new DefaultCachingProvider();
			cfg.CompilerServiceFactory = new DefaultCompilerServiceFactory();
			Engine.Razor = RazorEngineService.Create(cfg);
		}

		public static void Template(Request rq, string TemplateFileName, Type ModelType, object Model)
		{
			string result = RazorHelper.Template(TemplateFileName, ModelType, Model);

			rq.Response.Buffering = Response.BufferModes.None;
			rq.Response.SetStatus(200);
			rq.Response.AddHeader("Content-Type", "text/html");
			rq.Response.AddHeader("Content-Length", result.Length.ToString(CultureInfo.InvariantCulture));
			rq.Response.Write(result);
			rq.Close();
		}

		public static string Template(string TemplateFileName, Type ModelType, object Model)
		{
			return Engine.Razor.RunCompile(new NameOnlyTemplateKey(TemplateFileName, ResolveType.Global, null), ModelType, Model);
		}
		#endregion
	}
}
