﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proweb
{
	public static class ProWebHelper
	{
		#region Methods
		public static string ToStringHttp(this DateTime dt)
		{
			return dt.ToString("ddd, dd-MMM-yyyy HH:mm:ss") + " GMT";
		}
		#endregion
	}
}
